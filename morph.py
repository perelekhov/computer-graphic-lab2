import numpy as np
from skimage import io
from matplotlib import pyplot as plt
import morphyProcessing as mp
images = ['img/circle.jpeg', 'img/triangle.jpeg', 'img/square.jpeg', 'img/circle_empty.png']
rows = len(images)
for name in images:
    img = io.imread(name, as_gray=True)
    imgBinary = mp.convert_to_binary(img)
    structEl = np.ones((3, 3), dtype=np.int64)
    dilation = mp.apply_dilation(imgBinary, structEl)
    erosion = mp.apply_erosion(imgBinary, structEl)
    w=10
    h=10
    fig=plt.figure(figsize=(8, 8))
    columns = 6

    # dilation = dilation(img>12, structEl)
    close = mp.apply_erosion(dilation, structEl)
    opens = mp.apply_dilation(erosion, structEl)
    imgs = [dilation, erosion,opens, close, imgBinary]
    labels = ["Dilation", "Erosion","Opens", "close", "Orig Bin image"]
    # imgs = [erosion, imgBinary]
    for i in range(0, len(imgs)):
        img = imgs[i]
        plot = fig.add_subplot(rows, columns, i + 1)
        plot.title.set_text(labels[i])
        plt.imshow(img)
plt.show()