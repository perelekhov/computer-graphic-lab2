from itertools import product
import numpy as np


def convert_to_binary(image):
    width, height = image.shape
    prod_iter = product(range(width), range(height))
    img_result = np.zeros_like(image)
    for i,j in prod_iter:
        if image[i][j] < image.max() / 2:
            pixel_value = 1
        else:
            pixel_value = 0
        img_result[i][j] = pixel_value;
    return img_result


def _calculate_applied_matrix(elOne, elTwo, operation='OR'):
    width, height = elOne.shape
    prod_iter = product(range(width), range(height))
    img_result = np.zeros_like(elOne)
    for i,j in prod_iter:
        result_pixel = 0
        if operation == 'OR':
            result_pixel = ext_based_on_element(elTwo, elOne, 'max');
        if operation == 'AND':
            result_pixel = ext_based_on_element(elTwo, elOne, 'min');
        img_result[i][j] = result_pixel
    return img_result


def apply_dilation(image, structEl):
    el_width, el_height = structEl.shape
    width, height = image.shape
    prod_iter = product(range(width), range(height))
    img_result = np.zeros_like(image)
    for i,j in prod_iter:
        if image[i][j] == 1:
            i_to = min(width, i + el_width)
            j_to = min(height, j + el_height)
            space_left_i, space_left_j = img_result[i:i_to, j:j_to].shape
            img_result[i:i_to, j:j_to] = _calculate_applied_matrix(
                image[i:i_to, j:j_to],
                structEl[0:space_left_i, 0:space_left_j],
                "OR"
            )
    return img_result


def ext_based_on_element(structEl, img, type='max'):
    buff = []
    meaning_pixels = collect_meaning_pixels(structEl)
    width, height = img.shape
    prod_iter = product(range(width), range(height))
    for i,j in prod_iter:
        if (i,j) in meaning_pixels:
            buff.append(img[i][j])
    if type == 'max':
        return np.array(buff).max()
    if type == 'min':
        return np.array(buff).min()


def collect_meaning_pixels(structEl):
    width, height = structEl.shape
    prod_iter = product(range(width), range(height))
    result = []
    for i,j in prod_iter:
        if structEl[i][j] == 1:
            result.append((i,j))
    return result


def apply_erosion(image, structEl):
    el_width, el_height = structEl.shape
    width, height = image.shape
    prod_iter = product(range(width), range(height))
    img_result = np.zeros_like(image)
    for i,j in prod_iter:
        if image[i][j] == 1:
            i_to = min(width, i + el_width)
            j_to = min(height, j + el_height)
            space_left_i, space_left_j = img_result[i:i_to, j:j_to].shape
            img_result[i:i_to, j:j_to] = _calculate_applied_matrix(
                image[i:i_to, j:j_to],
                structEl[0:space_left_i, 0:space_left_j],
                "AND"
            )
    return img_result